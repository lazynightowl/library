var app = app || {};

app.LibraryView = Backbone.View.extend({
    el: '.trig', // class wrapper for 3 click events
    events:{
        "click #add":"addBook",
        "click #reset":"reset",
        "click #populate":"populate"
    },
    
    initialize: function( initialBooks ) {
        console.debug("LibraryView initialize()")
        this.collection = new app.Library( initialBooks );
        
        this.render();

        // this.listenTo( this.collection, 'add', this.renderBook );        
    },

    render: function() {
 
        /* custom cell to delete row, model and view of each book in grid */
        var DeleteCell = Backgrid.Cell.extend({
          className: "remove-cell",
          template: _.template(" <button type=\"button\" class=\"btn btn-sm btn-danger delete\"><i class=\"fa fa-trash-o fa-lg\"></i> Delete</button> "),
          events: {
            "click": "deleteRow"
          },
          deleteRow: function (e) {
            e.preventDefault();
            if(this.model.collection.length-1 <= 0){
                $(".backgrid").hide();
                $(".backgrid-filter").hide();
            } 

            this.model.collection.remove(this.model);


  
          },
          render: function () {
            this.$el.html(this.template());
            this.delegateEvents();
            return this;
          }
        });

        /* columns definition for Backgrid.js grid */
        var columns = [
        {
            name: "title",
            label: "Title",
            cell: "string",
            sortType: "toggle",
            sort: "ascending"
        }, {
            name: "author",
            label: "Author",
            cell: "string",
            sortType: "toggle"
        },
        {
            name: "",
            label: "Actions",
            editable: false,
            sortable: false,
            cell: DeleteCell

        }];

        /* init a new grid instance */
        var grid = new Backgrid.Grid({
            columns: columns,
            collection: this.collection
        });
        
        /* client-side filter */
        var filter = new Backgrid.Extension.ClientSideFilter({
          collection: this.collection,
          placeholder: "search for titles and authors",
          fields: ['title','author']
        });

        /* render the grid */
        $("#library").append(grid.render().el);

        /* render the filter */
        $("#library").before(filter.render().el);

    },


    renderBook: function( item ) {
        var bookView = new app.BookView({
            model: item
        });
        this.$el.append( bookView.render().el );
    },
    
    addBook: function( e ) {
    e.preventDefault();
    var formData = {};

    $( '#addBook div' ).children( 'input' ).each( function( i, el ) {
        if( $( el ).val() != '' )
        {
            formData[ el.id ] = $( el ).val();
        }
    });
      console.debug("Add New Book - Title: "+formData['title'] +" Author:"+formData['author']);
      this.collection.add( new app.Book( formData ) );
      $(".backgrid").show();
      $(".backgrid-filter").show();
    },

    populate: function( e ) {
      e.preventDefault();
      
      /* backbone.js documentation says I shouldn’t call fetch explicitly on page load, 
         but this is just for testing purposes  should use a bind event technique */
      this.collection.fetch({remove: false}); 
      $(".backgrid").show();
      $(".backgrid-filter").show();
      console.debug("Populated library with more 100 classics books.");
    },

    reset: function( e ) {
      e.preventDefault();
      console.debug("Reset: deleting "+this.collection.length+" books!");
      this.collection.reset();
      if(this.collection.length <= 0){
          $(".backgrid").hide();
          $(".backgrid-filter").hide();
          //console.clear();
      } 
      console.debug("Library with "+this.collection.length+" books now!");
  
    }

});
