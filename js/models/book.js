var app = app || {};

app.Book = Backbone.Model.extend({
	defaults: {
		title: 'Untitled',
		author: 'Unknown'
	}
});