var app = app || {}; // define app as an empty object if it's not already defined

$(function() {
	console.clear();
	console.log("");
	console.log("%c Welcome to YARAKU LIBRARY v.1","font-weight: bold; color: black;");
	console.log("developed by João Amaral in Aug 2014 for Yaraku, Inc.");
	console.log("This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.");
	console.log("%c To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to ", "font-style: italic;");
	console.log("%c   Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.", "font-style: italic;");
	console.log("");
	console.log("%c TODOs","font-weight: bold; color: black;");
	console.log(" > Validation on Grid");
	console.log(" > Validation on Form (Modal)");
	console.log(" > Bind event to collection.fetch()");
	console.log("");
 	var someBooks = [
        { title: 'Pride and Prejudice', author: 'Jane Austen'},
        { title: 'Alice\'s Adventures in Wonderland', author: 'Lewis Carol'},
        { title: 'Adventures of Tom Sawyer', author: 'Mark Twain'},
        { title: 'Dragons of Eden', author:'Carl Sagan'}
    ];
    new app.LibraryView( someBooks );
});